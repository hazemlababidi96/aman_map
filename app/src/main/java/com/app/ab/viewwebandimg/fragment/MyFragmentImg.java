package com.app.ab.viewwebandimg.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ab.viewwebandimg.R;
import com.squareup.picasso.Picasso;

import androidx.fragment.app.Fragment;

public class MyFragmentImg extends Fragment {
    public static final String IMAGE_MESSAGE = "IMAGE_MESSAGE";


    public static final MyFragmentImg newInstance( String img)
    {
        MyFragmentImg fragment = new MyFragmentImg();
        Bundle bdl = new Bundle();
        bdl.putString(IMAGE_MESSAGE,img);
        fragment.setArguments(bdl);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.myfragmentimg_layout, container, false);

        String img = getArguments().getString(IMAGE_MESSAGE);

        ImageView image = (ImageView)v.findViewById(R.id.imageView_myfragment_layout_home_screen);
       // image.setImageResource(img);
        Picasso.get().load(img).error(R.drawable.ic_launcher_foreground).into(image);


        return v;
    }
}
