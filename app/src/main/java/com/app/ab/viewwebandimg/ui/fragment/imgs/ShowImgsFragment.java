package com.app.ab.viewwebandimg.ui.fragment.imgs;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.MynewPageAdapter;
import com.app.ab.viewwebandimg.fragment.MyFragmentImg;
import com.app.ab.viewwebandimg.model.ItemForGame;
import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class ShowImgsFragment extends Fragment {

    private View view;
    private ViewPager2 viewPager;
    private PageIndicatorView pageIndicatorView;
    private MynewPageAdapter pageAdapter;
    private List<Fragment> fragments;
    private List<String> img = new ArrayList<>();


    public ShowImgsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.fragment_show_imgs, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        ItemForGame item = ShowImgsFragmentArgs.fromBundle(getArguments()).getData();

        img = item.getImg();

        fragments = getFragments();
        pageAdapter = new MynewPageAdapter(getActivity(), fragments);
        viewPager = view.findViewById(R.id.viewPager);
        viewPager.setAdapter(pageAdapter);
        viewPager.setBackgroundColor(Color.WHITE);
        pageIndicatorView.setSelectedColor(Color.rgb(112,112,112));

        pageIndicatorView.setCount(fragments.size());

       // viewPager.setCurrentItem(1);


        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                pageIndicatorView.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);

            }
        });





    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        if(img!=null)
        {
            for(int i=0;i<img.size();i++)
            {
                fList.add(MyFragmentImg.newInstance((img.get(i))));

            }
        }


        return fList;
    }

    private void initView() {
        viewPager =  view.findViewById(R.id.viewPager);
        pageIndicatorView =  view.findViewById(R.id.pageIndicatorView);
    }

    @Override
    public void onStop() {
        super.onStop();

    }

}