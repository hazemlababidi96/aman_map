package com.app.ab.viewwebandimg.ui.fragment.audio;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.AdapterShowGame;
import com.app.ab.viewwebandimg.model.ItemForGame;

import java.util.ArrayList;
import java.util.List;


public class ListShowAudioFragment extends Fragment {


    private View view;

    private RecyclerView recyclerView;
    private AdapterShowGame adapterShowGame;
    private ArrayList<ItemForGame> itemForGames = new ArrayList<>();
    private AlphaInAnimationAdapter alphaAdapter;
    public ListShowAudioFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.listfragment_show_audio, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        GetDataAudio();
        recyclerView = (RecyclerView) view.findViewById(R.id.recy);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterShowGame = new AdapterShowGame( itemForGames);
        recyclerView.setAdapter(adapterShowGame);
        alphaAdapter = new AlphaInAnimationAdapter(adapterShowGame);
        recyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
    }

    private void GetDataAudio() {

        List<String> strings = new ArrayList<>();
        strings.add("https://www.mexatk.com/wp-content/uploads/2017/05/%D8%A7%D8%AD%D9%84%D9%8A-%D8%A7%D8%B7%D9%81%D8%A7%D9%84-1-800x534.jpg");
        strings.add("https://cdn.al-ain.com/images/2019/2/13/154-134452-france-parties-congenital-defects-investigation_700x400.jpeg");
        strings.add("https://i.pinimg.com/564x/7e/82/d2/7e82d269f84f509c08e214693df6ef82.jpg");
        itemForGames.add(new ItemForGame("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3", "audio", strings, "يُحكى أنه كان يعيش طفل صغير له ظهر مقوس، وعلى الرغم من ذلك إلا انه كان ماهرا في عمله، كما أنه كان متميز فى الرمح ورمي السهام، ولذلك أراد أن ينضم إلى جيش الملك على الرغم من صغر سنه، لكنه عرف أنه لن يُسمح له بالقيام بذلك بسبب مكانته الصغيرة وسنه .\n" +
                "\n" +
                " \n" +
                "\n" +
                "لم ييأس الطفل الصغير، وقرر أن يبحث عن رجل ضخم وقوي، وقال فى نفسه عندما سأجد رجلا قويا سوف أطلب منه أن ياخذني لمساعدته، ثم بعدها بالتأكيد سوف نذهب الى القصر، وبالتأكيد سوف يسمح الملك للرجل الضخم ومساعديه أن ينضموا إلى الجيش، ومن الممكن أن أبرم صفقة مع هذا الرجل القوي .\n" +
                "\n" +
                " \n" +
                "\n" +
                "بالفعل بدأ الطفل الصغير فى البحث عن رجل قوي، وسرعان ما وجد أحد الرجال يحفر خندقا، فسأل الطفل الصغير \" لماذا تحفر تلك الحفرة الكبيرة يا سيدي؟\" فأخبره الرجل القوي أن هذه هى الطريقة الوحيدة لكسب رزقه من الملك، فقال له الطفل الصغير \"ما رأيك إذا قمت بمساعدتك وتعملت كل أعمالك وتشاركما الأجر؟\" وافق الرجل القوى، وبالفعل أخذه الى قصر الملك .","الطفل الصغير"));


    }

    @Override
    public void onStop() {
        super.onStop();
        itemForGames.clear();
    }
}