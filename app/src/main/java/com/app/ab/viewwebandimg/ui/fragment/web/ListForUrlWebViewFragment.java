package com.app.ab.viewwebandimg.ui.fragment.web;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.AdapterShowGame;
import com.app.ab.viewwebandimg.adapter.AdapterUrlWeb;
import com.app.ab.viewwebandimg.model.ItemForGame;
import com.app.ab.viewwebandimg.model.ItemUrlGame;

import java.util.ArrayList;


public class ListForUrlWebViewFragment extends Fragment {

    private View view;

    private RecyclerView recyclerView;
    private AdapterUrlWeb adapterUrlWeb;
    private ArrayList<ItemUrlGame> itemUrlGames = new ArrayList<>();
    private AlphaInAnimationAdapter alphaAdapter;



    public ListForUrlWebViewFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.fragment_list_for_url_web_view, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // getArguments().getString("nameUrl");
     //   Log.d("uuu name frag",getArguments().getInt("nameUrl")+"");

        GetData();

        recyclerView = (RecyclerView) view.findViewById(R.id.recy);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterUrlWeb = new AdapterUrlWeb( itemUrlGames);
        recyclerView.setAdapter(adapterUrlWeb);
        alphaAdapter = new AlphaInAnimationAdapter(adapterUrlWeb);
        recyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
    }

    private void GetData() {

        ItemUrlGame url=new ItemUrlGame();
        url.setNameGame("كورونا");
        url.setUrl("https://wordwall.net/ar/resource/7459982");

        ItemUrlGame url1=new ItemUrlGame();
        url1.setNameGame("فايروس كورونا");
        url1.setUrl("https://wordwall.net/ar/resource/7460820");

        ItemUrlGame url2=new ItemUrlGame();
        url2.setNameGame("كورونا");
        url2.setUrl("https://wordwall.net/ar/resource/4086284");

        ItemUrlGame url3=new ItemUrlGame();
        url3.setNameGame("اوجد اساليب الوقاية");
        url3.setUrl("https://wordwall.net/ar/resource/8984738");

        ItemUrlGame url4=new ItemUrlGame();
        url4.setNameGame("كيف أحافظ علي نفسي من فيروس كورونا");
        url4.setUrl("https://wordwall.net/ar/resource/8985991");

        itemUrlGames.add(url);
        itemUrlGames.add(url1);
        itemUrlGames.add(url2);
        itemUrlGames.add(url3);
        itemUrlGames.add(url4);

    }

    @Override
    public void onStop() {
        super.onStop();
        itemUrlGames.clear();
    }
}