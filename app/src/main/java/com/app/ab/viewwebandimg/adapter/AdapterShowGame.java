package com.app.ab.viewwebandimg.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.model.ItemForGame;
import com.app.ab.viewwebandimg.ui.fragment.ShowSectionsFragmentDirections;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


public class AdapterShowGame extends RecyclerView.Adapter<AdapterShowGame.ViewHolderShowGame> {


    private List<ItemForGame> itemList = new ArrayList<>();

    private String typeclick,urlclick,detailsclick,titleclick;
    private List<String> imgclick = new ArrayList<>();

    public AdapterShowGame(List<ItemForGame> itemList) {
        this.itemList = itemList;

    }

    @NonNull
    @Override
    public ViewHolderShowGame onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);

        ViewHolderShowGame holder = new ViewHolderShowGame(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderShowGame holder, int position) {
        final ItemForGame vvv = itemList.get(position);

        final ViewHolderShowGame myHolder = (ViewHolderShowGame) holder;

        Log.d("ttt", itemList.size() + "");


        if (vvv.getType().equals("youtube"))
        {
            myHolder.textView.setText(vvv.getTitel());
            Picasso.get().load(R.mipmap.ic_play).resize(70, 70).error(R.drawable.ic_launcher_foreground).into(myHolder.imageViewtype);
            if(vvv.getImg()!=null)
            {
                Picasso.get().load(vvv.getImg().get(0)).resize(300, 300).error(R.drawable.ic_launcher_foreground).into(myHolder.imageView);
            }
        } else if (vvv.getType().equals("img"))  {
            myHolder.textView.setText(vvv.getTitel());
            Picasso.get().load(R.mipmap.ic_img).resize(70, 70).error(R.drawable.ic_launcher_foreground).into(myHolder.imageViewtype);
            if(vvv.getImg()!=null)
            {
                Picasso.get().load(vvv.getImg().get(0)).resize(300, 300).error(R.drawable.ic_launcher_foreground).into(myHolder.imageView);
            }
        }
        else if (vvv.getType().equals("txtimg"))
        {
            myHolder.textView.setText(vvv.getTitel());
            Picasso.get().load(R.mipmap.ic_img).resize(70, 70).error(R.drawable.ic_launcher_foreground).into(myHolder.imageViewtype);
            if(vvv.getImg()!=null)
            {
                Picasso.get().load(vvv.getImg().get(0)).resize(300, 300).error(R.drawable.ic_launcher_foreground).into(myHolder.imageView);
            }
        }
        else if (vvv.getType().equals("audio"))
        {
            myHolder.textView.setText(vvv.getTitel());
            Picasso.get().load(R.mipmap.ic_audio).resize(70, 70).error(R.drawable.ic_launcher_foreground).into(myHolder.imageViewtype);
            if(vvv.getImg()!=null)
            {
                Picasso.get().load(vvv.getImg().get(0)).resize(300, 300).error(R.drawable.ic_launcher_foreground).into(myHolder.imageView);
            }
        }

        myHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                titleclick =vvv.getTitel();
                typeclick = vvv.getType();
                urlclick =vvv.getUrlyoutubeoraoudio();
                detailsclick=vvv.getDetails();
                imgclick =  vvv.getImg();

//                Toast.makeText(myHolder.itemView.getContext(), titleclick +" / "+typeclick
//                        +" / "+urlclick
//                        +" / "+detailsclick, Toast.LENGTH_SHORT).show();

                ItemForGame item = new ItemForGame(urlclick, typeclick, imgclick, detailsclick, titleclick);


                if(typeclick.equals("img"))
                {
                    NavDirections navDirections = ShowSectionsFragmentDirections.actionShowSectionsFragmentToShowImgsFragment(item)
                            .setData(item);
                    Navigation.findNavController(v).navigate(navDirections);
                }
                else if(typeclick.equals("txtimg"))
                {
                    NavDirections navDirections = ShowSectionsFragmentDirections.actionShowSectionsFragmentToShowImgTxtFragment(item)
                            .setData(item);
                    Navigation.findNavController(v).navigate(navDirections);
                }
                else if(typeclick.equals("youtube"))
                {
                    NavDirections navDirections = ShowSectionsFragmentDirections.actionShowSectionsFragmentToShowVideoFragment(item)
                            .setData(item);
                    Navigation.findNavController(v).navigate(navDirections);
                }
                else if(typeclick.equals("audio"))
                {
                    NavDirections navDirections = ShowSectionsFragmentDirections.actionShowSectionsFragmentToShowAudioFragment(item)
                            .setData(item);
                    Navigation.findNavController(v).navigate(navDirections);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setList(List<ItemForGame> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }


    public class ViewHolderShowGame extends RecyclerView.ViewHolder {

        ImageView imageView,imageViewtype;
        TextView textView;
        public ViewHolderShowGame(@NonNull View itemView) {
            super(itemView);
            textView= itemView.findViewById(R.id.textvitem);
            imageView=itemView.findViewById(R.id.img);
            imageViewtype=itemView.findViewById(R.id.img_type);

        }
    }

}

