package com.app.ab.viewwebandimg.ui.fragment.video;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.MynewPageAdapter;
import com.app.ab.viewwebandimg.fragment.MyFragmentImg;
import com.app.ab.viewwebandimg.model.ItemForGame;
import com.rd.PageIndicatorView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ShowVideoFragment extends Fragment {

    private View view;

    private TextView txtDetails;
    private ImageView imgplay;
    private ImageView imgvideo;

    public ShowVideoFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        view =inflater.inflate(R.layout.fragment_show_video, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();

        ItemForGame item = ShowVideoFragmentArgs.fromBundle(getArguments()).getData();

        txtDetails.setText(item.getDetails());
        Picasso.get().load(item.getImg().get(0)).error(R.drawable.ic_launcher_foreground).into(imgvideo);



        imgplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    NavDirections navDirections = ShowVideoFragmentDirections.actionShowVideoFragmentToShowYotubeFragment()
                            .setUrlyoutube(item.getUrlyoutubeoraoudio());
                    Navigation.findNavController(v).navigate(navDirections);


            }
        });
    }


    private void initView() {
        txtDetails =  view.findViewById(R.id.txt_details);
        imgplay =  view.findViewById(R.id.img_play);
        imgvideo =  view.findViewById(R.id.img_video);

    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
