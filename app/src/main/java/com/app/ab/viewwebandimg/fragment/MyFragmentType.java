package com.app.ab.viewwebandimg.fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ab.viewwebandimg.R;

import androidx.fragment.app.Fragment;

public class MyFragmentType extends Fragment {
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String EXTRA_MESSAGE1 = "EXTRA_MESSAGE1";
    public static final String IMAGE_MESSAGE = "IMAGE_MESSAGE";


    public static final MyFragmentType newInstance(String message, String message1, int img) {
        MyFragmentType fragment = new MyFragmentType();
        Bundle bdl = new Bundle();
        bdl.putString(EXTRA_MESSAGE, message);
        bdl.putString(EXTRA_MESSAGE1, message1);
        bdl.putInt(IMAGE_MESSAGE, img);
        fragment.setArguments(bdl);
        Log.d("ttt newInstance", img + "");
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String message = getArguments().getString(EXTRA_MESSAGE);
        String message1 = getArguments().getString(EXTRA_MESSAGE1);

        View v = inflater.inflate(R.layout.myfragment_layout, container, false);

        TextView messageTextView = (TextView) v.findViewById(R.id.textView_myfragment_layout);
        TextView messageTextView1 = (TextView) v.findViewById(R.id.textView1_myfragment_layout);

        messageTextView.setText(message);
        messageTextView1.setText(message1);

        int img = getArguments().getInt(IMAGE_MESSAGE);
        Log.d("ttt img", img + "");
        ImageView image = (ImageView) v.findViewById(R.id.imageView_myfragment_layout_home_screen);
        image.setImageResource(img);


        return v;
    }
}
