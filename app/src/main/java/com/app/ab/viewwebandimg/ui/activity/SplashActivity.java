package com.app.ab.viewwebandimg.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.config.AppMain;
import com.app.ab.viewwebandimg.config.SharedPref;
import com.kaopiz.kprogresshud.KProgressHUD;

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (mWifi.isConnected()) {
            showHome();

        } else {
            Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();

        }

    }

    private void showHome() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppMain.getInstance().getSharedPref().readBoolean(SharedPref.APP_IS_LOGIN)) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    Intent i = new Intent(SplashActivity.this, TourActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        }, 2500);

    }


}