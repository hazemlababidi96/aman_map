package com.app.ab.viewwebandimg.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.config.AppMain;
import com.app.ab.viewwebandimg.config.SharedPref;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppMain.getInstance().getSharedPref().writeBoolean(SharedPref.APP_IS_LOGIN,true);
    }
}