package com.app.ab.viewwebandimg.adapter;

import com.app.ab.viewwebandimg.ui.fragment.audio.ListShowAudioFragment;
import com.app.ab.viewwebandimg.ui.fragment.txtimg.ListShowIMgTxtFragment;
import com.app.ab.viewwebandimg.ui.fragment.imgs.ListShowImgsFragment;
import com.app.ab.viewwebandimg.ui.fragment.video.ListShowVideoFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerAdapter extends FragmentStateAdapter {



   public List<String> namelist;
    private ArrayList<Fragment> arrayList = new ArrayList<>();


    public ViewPagerAdapter(FragmentActivity fa, List<String> namelist){//Pager constructor receives Activity instance
        super(fa);
        this.namelist = namelist;

    }

    @Override
    public int getItemCount() {
        return 4;
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {

//        String name = namelist.get(position);
//
//        ListForGameFragment myImageFragment= new ListForGameFragment();
//        Bundle bundle = new Bundle();
//        bundle.putInt("position", position);
//        bundle.putString("name", name);
//        myImageFragment.setArguments(bundle);
        switch (position) {
            case 0:
                return new ListShowImgsFragment();
            case 1:
                return new ListShowVideoFragment();
            case 2:
                return new ListShowAudioFragment();
            case 3:
                return new ListShowIMgTxtFragment();

        }
        return null;



        //myImageFragment.ItemSelectedonRecy(namelist.get(position));
       // return new ListForGameFragment();
       // return myImageFragment;
    }





}
