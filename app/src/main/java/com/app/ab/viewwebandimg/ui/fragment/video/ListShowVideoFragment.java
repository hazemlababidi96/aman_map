package com.app.ab.viewwebandimg.ui.fragment.video;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.AdapterShowGame;
import com.app.ab.viewwebandimg.model.ItemForGame;

import java.util.ArrayList;
import java.util.List;


public class ListShowVideoFragment extends Fragment {

    private View view;

    private RecyclerView recyclerView;
    private AdapterShowGame adapterShowGame;
    private ArrayList<ItemForGame> itemForGames = new ArrayList<>();
    private AlphaInAnimationAdapter alphaAdapter;
    public ListShowVideoFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.listfragment_show_video, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        GetDataVideo();

        recyclerView = (RecyclerView) view.findViewById(R.id.recy);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterShowGame = new AdapterShowGame( itemForGames);
        recyclerView.setAdapter(adapterShowGame);
        alphaAdapter = new AlphaInAnimationAdapter(adapterShowGame);
        recyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
    }

    private void GetDataVideo() {
        List<String> strings2 = new ArrayList<>();
        strings2.add("https://i.pinimg.com/736x/fb/c4/79/fbc479d53dc67d262434c1341e9eb63b.jpg");
        itemForGames.add(new ItemForGame("U629SjMCs2Y", "youtube", strings2,
                "هذه مجموعة قصص للأطفال عن حيوانات الغابة، وفي هذه القصة نحكي حكاية ؟ هذا ما ستعرفونه عند متابعة حلقة الأرنب النشيط و الأرنب الكسول من المسلسل الكرتوني زاد الحكايا\n" +
                "\n" +
                "قصص أطفال - قصص قبل النوم\n" +
                "تتميز القصص والحكايات على قناتنا عن غيرها من حيث:\n" +
                " - هذه السلسلة من قصص الأطفال ممتعة ومسلّية ومليئة بالفائدة والمعلومات القيمة.\n" +
                " - هذه القصص باللغة العربية الفصحى السليمة بعيداً عن اللهجات العامية وذلك لتعليم الطفل لغته العربية الفصحى.\n" +
                "-  هي من إنتاجنا بشكل كامل من التأليف إلى الرسم والتحريك والدوبلاج والمونتاج والإخراج، لذلك تم الإهتمام بكافة التفاصيل المتعلقة بقيمنا وخصوصيتنا.\n" +
                " - تنمي القيم لدى أطفالنا بعيداً عن العنف والقيم السلبية الموجودة في برامج الأطفال الأجنبية المدبلجة إلى اللغة العربية.\n","الأرنب النشيط و الأرنب الكسول"));

    }

    @Override
    public void onStop() {
        super.onStop();
        itemForGames.clear();
    }
}