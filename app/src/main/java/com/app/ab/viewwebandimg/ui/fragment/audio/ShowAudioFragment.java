package com.app.ab.viewwebandimg.ui.fragment.audio;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.MynewPageAdapter;
import com.app.ab.viewwebandimg.fragment.MyFragmentImg;
import com.app.ab.viewwebandimg.model.ItemForGame;
import com.rd.PageIndicatorView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ShowAudioFragment extends Fragment {

    private View view;

    private TextView txtDetails;
    private ImageView imgplay;

    MediaPlayer mediaPlayer;

    public ShowAudioFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.fragment_show_audio, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();

        ItemForGame item = ShowAudioFragmentArgs.fromBundle(getArguments()).getData();


        txtDetails.setText(item.getDetails());

        imgplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mediaPlayer!=null)
                {
                    if(mediaPlayer.isPlaying())
                    {
                        mediaPlayer.stop();
                        Picasso.get().load(R.mipmap.ic_play).error(R.drawable.ic_launcher_foreground).into(imgplay);
                        return;
                    }
                }

                Picasso.get().load(R.mipmap.ic_stop).error(R.drawable.ic_launcher_foreground).into(imgplay);
                mediaPlayer  = MediaPlayer.create(getActivity(), Uri.parse(item.getUrlyoutubeoraoudio())) ;
                mediaPlayer.start();




            }
        });
    }


    private void initView() {

        txtDetails =  view.findViewById(R.id.txt_details);
        imgplay =  view.findViewById(R.id.img_play);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mediaPlayer!=null)
        {
            if (mediaPlayer.isPlaying())
            {
                mediaPlayer.stop();
            }
        }

    }
}