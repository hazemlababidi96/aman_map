package com.app.ab.viewwebandimg.model;

import java.io.Serializable;
import java.util.List;

public class ItemForGame implements Serializable {

    private String urlyoutubeoraoudio;
    private String type;
    private List<String> img;
    private String details;
    private String titel;

    public ItemForGame(String urlyoutubeoraoudio, String type, List<String> img, String details, String titel) {
        this.urlyoutubeoraoudio = urlyoutubeoraoudio;
        this.type = type;
        this.img = img;
        this.details = details;
        this.titel = titel;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getUrlyoutubeoraoudio() {
        return urlyoutubeoraoudio;
    }

    public void setUrlyoutubeoraoudio(String urlyoutubeoraoudio) {
        this.urlyoutubeoraoudio = urlyoutubeoraoudio;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getImg() {
        return img;
    }

    public void setImg(List<String> img) {
        this.img = img;
    }


}
