package com.app.ab.viewwebandimg.ui.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.MynewPageAdapter;
import com.app.ab.viewwebandimg.fragment.MyFragment;
import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class TourActivity extends AppCompatActivity {

    private MynewPageAdapter pageAdapter;
    private PageIndicatorView pageIndicatorView;
    private ViewPager2 pager;
    private List<Fragment> fragments;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(getResources().getColor(R.color.but_white));
        setContentView(R.layout.activity_tour);
        initView();
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        fList.add(MyFragment.newInstance("اقرأ قصص مميزة و جديدة", "",R.drawable.one_splash));
        fList.add(MyFragment.newInstance("شاهد قصص جميلة", "", R.drawable.two_splash));
        fList.add(MyFragment.newInstance("تعلم من خلال اللعب", "", R.drawable.three_splash));

        return fList;
    }

    private void initView() {

        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        final TextView bntnext = findViewById(R.id.next);
        final TextView bntskap = findViewById(R.id.skip);
        final TextView txtend = findViewById(R.id.but_end);

        fragments = getFragments();
        pageAdapter = new MynewPageAdapter(this, fragments);
        pager = findViewById(R.id.viewPager);
        pager.setAdapter(pageAdapter);
        pager.setBackgroundColor(Color.WHITE);
        pageIndicatorView.setSelectedColor(Color.rgb(112,112,112));

        pager.setCurrentItem(3);

        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                pageIndicatorView.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
                if (pager.getCurrentItem() == 0) {
                    txtend.setVisibility(View.VISIBLE);
                    bntnext.setVisibility(View.GONE);
                    bntskap.setVisibility(View.GONE);
                } else {
                    txtend.setVisibility(View.GONE);
                    bntnext.setVisibility(View.VISIBLE);
                    bntskap.setVisibility(View.VISIBLE);
                }
            }
        });


        txtend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TourActivity.this, MainActivity.class);
                startActivity(i);
                finish();

            }
        });
        bntskap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TourActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        bntnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                pager.setCurrentItem(getItem(-1), true);
//                if (pager.getCurrentItem() == 0) {
//
//                    bntskap.setVisibility(View.GONE);
//                }

                pager.setCurrentItem(getItem(-1), true);

                if (pager.getCurrentItem() == 0) {
                    txtend.setVisibility(View.VISIBLE);
                    bntnext.setVisibility(View.GONE);
                    bntskap.setVisibility(View.GONE);
                }
            }
        });


    }

    private int getItem(int i) {
        return pager.getCurrentItem() + i;
    }
}