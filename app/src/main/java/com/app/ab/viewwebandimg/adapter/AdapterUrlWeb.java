package com.app.ab.viewwebandimg.adapter;

import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.model.ItemUrlGame;
import com.app.ab.viewwebandimg.ui.fragment.web.ListForUrlWebViewFragmentDirections;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


public class AdapterUrlWeb extends RecyclerView.Adapter<AdapterUrlWeb.ViewHolderUrlWeb> {


    private List<ItemUrlGame> itemList = new ArrayList<>();

    private String urlc;

    public AdapterUrlWeb(List<ItemUrlGame> itemList) {
        this.itemList = itemList;

    }

    @NonNull
    @Override
    public ViewHolderUrlWeb onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rowurl, parent, false);

        ViewHolderUrlWeb holder = new ViewHolderUrlWeb(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderUrlWeb holder, int position) {
        final ItemUrlGame vvv = itemList.get(position);

        final ViewHolderUrlWeb myHolder = (ViewHolderUrlWeb) holder;

        Log.d("ttt", itemList.size() + "");

        myHolder.textView.setText(vvv.getNameGame());

        myHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                urlc = vvv.getUrl();

                NavDirections navDirections = ListForUrlWebViewFragmentDirections.actionListForUrlWebViewFragmentToWebViewFragment()
                        .setUrl(urlc);
                Navigation.findNavController(v).navigate(navDirections);

            }
        });



    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setList(List<ItemUrlGame> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }


    public class ViewHolderUrlWeb extends RecyclerView.ViewHolder {

        TextView textView;

        public ViewHolderUrlWeb(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.textvitem);

        }
    }

}

