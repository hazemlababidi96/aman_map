package com.app.ab.viewwebandimg.ui.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.ViewPagerAdapter;
import com.app.ab.viewwebandimg.config.AppMain;
import com.app.ab.viewwebandimg.config.SharedPref;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.Arrays;
import java.util.List;


public class ShowSectionsFragment extends Fragment {


    ViewPager2 mViewPager;
    ViewPagerAdapter adapter;
    private View view;

    boolean isfarst=true;
    int clc=0;

    public ShowSectionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

            view =inflater.inflate(R.layout.fragment_show_sections, container, false);

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


            final List<String> nameUrl = Arrays.asList(new String[]{"مصورة", "مرئية", "مسموعة", "نصية"});


            mViewPager = (ViewPager2) getView().findViewById(R.id.viewPager2);
            adapter = new ViewPagerAdapter(getActivity(), nameUrl);
            mViewPager.setAdapter(adapter);

        mViewPager.setPageTransformer(new MarginPageTransformer(1500));
            

            TabLayout tabLayout = getView().findViewById(R.id.tabLayout);


            TabLayoutMediator tt = new TabLayoutMediator(tabLayout, mViewPager, new TabLayoutMediator.TabConfigurationStrategy() {
                @Override
                public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                    tab.setText(nameUrl.get(position));//Sets tabs names as mentioned in ViewPagerAdapter fragmentNames array, this can be implemented in many different ways.

                }
            });
            tt.attach();

            if(isfarst)
            {
                AppMain.getInstance().getSharedPref().writeString(SharedPref.APP_TYPE, "img");

                isfarst=false;
            }


//            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//                @Override
//                public void onTabSelected(TabLayout.Tab tab) {
//
//                    // Toast.makeText(getContext(), tab.getText()+"", Toast.LENGTH_SHORT).show();
//
//                    if(tab.getText().toString().equals("صور"))
//                    {
//                        AppMain.getInstance().getSharedPref().writeString(SharedPref.APP_TYPE, "img");
//
////                        mViewPager = (ViewPager2) view.findViewById(R.id.viewPager2);
////                        adapter = new ViewPagerAdapter(getActivity(), nameUrl);
////                        mViewPager.setAdapter(adapter);
//                    }
//                    else if(tab.getText().toString().equals("فيديو"))
//                    {
//                        AppMain.getInstance().getSharedPref().writeString(SharedPref.APP_TYPE, "video");
//
////                        mViewPager = (ViewPager2) view.findViewById(R.id.viewPager2);
////                        adapter = new ViewPagerAdapter(getActivity(), nameUrl);
////                        mViewPager.setAdapter(adapter);
//                    }
//                    else if(tab.getText().toString().equals("صوت"))
//                    {
//                        AppMain.getInstance().getSharedPref().writeString(SharedPref.APP_TYPE, "audio");
//
////                        mViewPager = (ViewPager2) view.findViewById(R.id.viewPager2);
////                        adapter = new ViewPagerAdapter(getActivity(), nameUrl);
////                        mViewPager.setAdapter(adapter);
//                    }
//                    else if(tab.getText().toString().equals("صور مع نص"))
//                    {
//                        AppMain.getInstance().getSharedPref().writeString(SharedPref.APP_TYPE, "txtimg");
//
////                        mViewPager = (ViewPager2) view.findViewById(R.id.viewPager2);
////                        adapter = new ViewPagerAdapter(getActivity(), nameUrl);
////                        mViewPager.setAdapter(adapter);
//
//                    }
//
//                    mViewPager = (ViewPager2) view.findViewById(R.id.viewPager2);
//                    adapter = new ViewPagerAdapter(getActivity(), nameUrl);
//                    mViewPager.setAdapter(adapter);
//
//
//                }
//
//                @Override
//                public void onTabUnselected(TabLayout.Tab tab) {
//
//                }
//
//                @Override
//                public void onTabReselected(TabLayout.Tab tab) {
//
//                }
//            });

          //  mViewPager.setUserInputEnabled(false);



//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener(new View.OnKeyListener() {
//
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if( keyCode == KeyEvent.KEYCODE_BACK )
//                {
//                    Toast.makeText(getContext(),"هل دود الخروج اضغط مرا اخرى" , Toast.LENGTH_SHORT).show();
//
//                    Log.d("GGG",clc+"");
//                    if(clc==2)
//                    {
//
//                        Fragment navhost =((AppCompatActivity) getContext()).getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
//                        NavController c = NavHostFragment.findNavController(navhost);
//                        c.navigate(R.id.homeFragment);
//
//                    }
//                    else
//                    {
////                        Fragment navhost =((AppCompatActivity) getContext()).getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
////                        NavController c = NavHostFragment.findNavController(navhost);
////                        c.navigate(R.id.showSectionsFragment);
//                    }
//                    clc+=1;
//
//
//
//
//                   // return true;
//                }
//                return false;
//            }
//        });


    }

    @Override
    public void onStop() {
        super.onStop();
    }
}