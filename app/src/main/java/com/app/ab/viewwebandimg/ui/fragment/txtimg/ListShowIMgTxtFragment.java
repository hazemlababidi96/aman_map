package com.app.ab.viewwebandimg.ui.fragment.txtimg;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.ab.viewwebandimg.R;
import com.app.ab.viewwebandimg.adapter.AdapterShowGame;
import com.app.ab.viewwebandimg.model.ItemForGame;

import java.util.ArrayList;
import java.util.List;


public class ListShowIMgTxtFragment extends Fragment {
    private View view;

    private RecyclerView recyclerView;
    private AdapterShowGame adapterShowGame;
    private ArrayList<ItemForGame> itemForGames = new ArrayList<>();
    private AlphaInAnimationAdapter alphaAdapter;

    public ListShowIMgTxtFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view =inflater.inflate(R.layout.listfragment_show_i_mg_txt, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        GetDataTxtImg();

        recyclerView = (RecyclerView) view.findViewById(R.id.recy);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterShowGame = new AdapterShowGame( itemForGames);
        recyclerView.setAdapter(adapterShowGame);
        alphaAdapter = new AlphaInAnimationAdapter(adapterShowGame);
        recyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
    }

    private void GetDataTxtImg() {

        List<String> strings = new ArrayList<>();
        strings.add("https://i.pinimg.com/564x/7e/82/d2/7e82d269f84f509c08e214693df6ef82.jpg");
        itemForGames.add(new ItemForGame("", "txtimg", strings, "سعاد بنت ذكية، ومجدة في دروسها،تحبها معلمتها كثيراً، وتفتخر بها أمام زميلاتها، المسألة الواحدة في الرياضيات، لا تستغرق معها إلا وقتاً قصيراً، تسبق زميلاتها في حلها، فتبتسم لها معلمتها مشجعة وتربت على كتفها قائلة لزميلاتها: – أتمنى أن تقتدين بزميلتكن سعاد، وتنافسنها في الدروس والتحصيل، فأنتن لستن أقل منها ذكاء وفطنة، ولكنها نشيطة مجدة في الدراسة، وأنتن تتراخين في تأدية الواجبات التي عليكن… رن جرس المدرسة، لتقضي الطالبات فرصة قصيرة، يسترحن فيها، ويلعبن، ويتبادلن الآراء والأفكار فيما بينهن. تحلقت الطالبات حول سعاد، هذه تسألها في مسألة صعبة، وتلك تسألها كيف توفق بين اللعب والدراسة.. وعن بُعد وقفت هندُ ترمق سعاد بحسد وغيرة، تقضم أظافرها من الغيظ، وهي تفكر بطريقة تبعد سعاد عن الجد والاجتهاد، فتنفر صديقاتها منها، وتبتعد عنها معلمتها فيخلوا لها الجو، لتحتل مكانة سعاد… اقتربت هند من سعاد وسألتها: ما رأيك في الفيلم الكرتوني الذي عرض في التلفاز البارحة ؟ أجابت سعاد في تساؤل : أيَّ فيلم هذا ؟ أنا لا أتابع مثل هذه الأفلام حتى لا أضيع وقتي. ضحكت هند وقالت: – ماذا .. ماذا تقولين، لا تتابعين برامج التلفاز ما هذا الجهل ؟! ألا تدرين أن في هذه البرامج متعة كبيرة و… قاطعتها سعاد: – متعة كبيرة وفائدة قليلة.. ردت هند ضاحكة: – لابد أنّ أمك تمنعك من مشاهدة التلفاز، لكي تساعديها في بعض أعمال التنظيف، وتحرمك من متعة التلفاز، كم أشفق عليك يا صديقتي.. انزعجت سعاد من كلام صديقتها، وفضلت تركها والذهاب إلى مكان آخر.. ولكن هنداً أخذت تطاردها من مكان إلى مكان آخر، وتزين لها التلفاز وتسليته الجميلة، وتقلل لها من أهمية الواجبات المتعبة التي تتفنن المعلمة في زيادتها وصعوبتها… بدأت سعاد تميل إلى هند وإلى أفكارها ومناقشاتها… ورويداً رويداً أخذت تقتنع بكلامها المعسول، ونصائحها الخاطئة، فأهملت بعض واجباتها المدرسية والمنزلية، وتابعت الفيلم الأول في التلفاز، ثم الفيلم الثاني وهكذا، حتى كادت لا تفارق التلفاز إلا قليلاً.. لاحظت أمها هذا التقصير والإهمال، فنصحتها فلم تأبه لنصح أمها، فاضطرت الأم إلى تأنيبها ومعاقبتها فلم تفلح، وأما معلمة سعاد فقد تألمت كثيراً لتراجع سعاد في دراستها، وحاولت نصحها ولكن بلا فائدة .. حينئذ قررت المعلمة مقاطعتها في الصف، وكذلك قاطعتها أمها في البيت وأهملتها.. شعرت سعاد بالخجل الشديد والحرج أمام زميلاتها في الصف، وإخوتها في البيت، فأدركت خطأها الكبير وقررت الابتعاد عن هند ووسوستها، فنالت رضى أمها ومعلمتها وحبهما.\n" +
                "\n"
               , "قصة سعاد و التلفاز"));

    }

    @Override
    public void onStop() {
        super.onStop();
        itemForGames.clear();
    }
}