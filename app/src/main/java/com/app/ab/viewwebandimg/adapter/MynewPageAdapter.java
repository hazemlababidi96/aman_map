package com.app.ab.viewwebandimg.adapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class MynewPageAdapter extends FragmentStateAdapter {

   private List<Fragment> fragments;

    public MynewPageAdapter(FragmentActivity fa, List<Fragment> fragments) {
        super(fa);
        this.fragments=fragments;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getItemCount() {
        return this.fragments.size();
    }
}