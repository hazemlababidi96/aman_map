package com.app.ab.viewwebandimg.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ab.viewwebandimg.R;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;


public class HomeFragment extends Fragment {

    private View view;
    private ImageView imgMenu;
    private TextView txtTitel;
    private LinearLayout butWebview;
    private LinearLayout butGoforgame;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();

        butWebview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections navDirections = HomeFragmentDirections.actionHomeFragmentToShowSectionsFragment();
                Navigation.findNavController(v).navigate(navDirections);
            }
        });

        butGoforgame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections navDirections = HomeFragmentDirections.actionHomeFragmentToListForUrlWebViewFragment();
                Navigation.findNavController(v).navigate(navDirections);
            }
        });

    }

    private void initView() {
        imgMenu = view.findViewById(R.id.img_menu);
        txtTitel = view.findViewById(R.id.txt_titel);
        butWebview = view.findViewById(R.id.but_webview);
        butGoforgame = view.findViewById(R.id.but_goforgame);
    }
}